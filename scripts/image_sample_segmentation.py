import torch as th
from torch import nn
from torch.nn import functional as F
import torch.nn.utils.spectral_norm as spectral_norm
import numpy as np
import torchvision.transforms as T
from torchvision.utils import save_image
from torch.utils import data
from torch import optim
from torch import distributed as dist

from PIL import Image
import argparse
import time

import os

from guided_diffusion.dataloader.dataset import CelebAMaskDataset
from guided_diffusion.segmentation import GeneratorSeg
from guided_diffusion.modelsSemantic.utils import *
from guided_diffusion.utils.distributed import (
    get_rank,
    synchronize,
    reduce_loss_dict,
)
from image_segmentation import *

import argparse
import os
import sys

import torch as th
import torch.distributed as dist

import cv2
import matplotlib.pyplot as plt

import scipy
import numpy as np

from guided_diffusion import dist_util, logger
from guided_diffusion.script_util import (
    NUM_CLASSES,
    model_and_diffusion_defaults,
    create_model_and_diffusion,
    add_dict_to_argparser,
    args_to_dict,
)
timestr = time.strftime("%Y%m%d-%H%M%S")

def main():
    device = 'cpu' if th.cuda.is_available() != True else 'cuda'
    args = create_argparser().parse_args()

    dist_util.setup_dist()
    logger.configure()

    logger.log("creating model and diffusion...")
    model, diffusion = create_model_and_diffusion(
        **args_to_dict(args, model_and_diffusion_defaults().keys())
    )
    #model.modelSummary()
    logger.log("model and diffusion created")
    model.load_state_dict(
        dist_util.load_state_dict(args.model_path, map_location="cpu")
    )
    logger.log("sending weights to GPU...")
    model.to(dist_util.dev())
    if args.use_fp16:
        model.convert_to_fp16()
    model.eval()

    logger.log("Create segmentation model")
    channels = 256
    seg_dim = 8
    style_dim = 256
    size = 256
    n_mlp = 8
    channel_multiplier = 2
    inChannels = 515
    outChannels = 1
    g_reg_ratio = args.g_reg_every / (args.g_reg_every + 1)
    
    transform = get_transformation(args)
    limit_size = args.limit_size
    size = 256

    img_dataset = CelebAMaskDataset(args, args.img_dataset_path, unlabel_transform=transform,
                                            limit_size=limit_size,
                                            is_label=True, phase='train',resolution=size)

    img_loader = data.DataLoader(
            img_dataset,
            batch_size=args.batch,
            sampler=data_sampler(img_dataset, shuffle=False, distributed=False),
            drop_last=True,
            pin_memory=True,
            num_workers=4,
        )
    seg_loader = sample_data(img_loader)
    
    segGen = GeneratorSeg(size,style_dim,n_mlp,inChannels,outChannels,False,seg_dim=seg_dim,channel_multiplier=channel_multiplier).to(device)
    g_optim = optim.Adam(
            segGen.parameters(),
            lr=args.lr * g_reg_ratio,
            betas=(0 ** g_reg_ratio, 0.99 ** g_reg_ratio),
        )
    requires_grad(segGen, True)     

    #datatsetImages = os.listdir(args.path_dataset)
    #datatsetImages.sort()

    logger.log("sampling...")
    all_intermediateLayers = []
    all_masks = []
    all_images = []
    all_labels = []
    #i = 0

    #while len(all_images) * args.batch_size < args.num_samples:
    for i in range(0,int(args.limit_size/args.batch)):
        model_kwargs = {}
        if args.class_cond:
            if args.dataset == 'imageNet-Dog':
                classes = th.from_numpy(np.array([args.imagenet_class], dtype=np.int64)).to(dist_util.dev())
                logger.log(f'Image class choosen: {classes}')
            else:
                classes = th.randint(
                    low=0, high=NUM_CLASSES, size=(args.batch_size,), device=dist_util.dev()
                )
            model_kwargs["y"] = classes
            sample_fn = (
                diffusion.p_sample_loop if not args.use_ddim else diffusion.ddim_sample_loop
            )
        if args.modified != True:
            sample = sample_fn(
                model,
                (args.batch_size, 3, args.image_size, args.image_size),
                clip_denoised=args.clip_denoised,
                model_kwargs=model_kwargs,
            )
        else:
            logger.log('Code modified')
            #print(datatsetImages[i])
            #args.image_to_segment = f'{args.path_dataset}/{datatsetImages[i]}'
            #print(f'Path image: {datatsetImages[i]}')
            #diffusion.quitaruido = True
            #diffusion.always_save = True
            index = args.timestep_to_segment
            num_timesteps = int(args.timestep_respacing)
            i_noise=num_timesteps-index
            q_noise=num_timesteps-index
            print(f'noise: {num_timesteps}-{index} = {q_noise}')
            log_noise=-1.5
            i_unet=num_timesteps-index
            
            #img = cv2.imread(args.image_to_segment)
            #print(f'Image shape cv2.imread: {img.shape}')
            #imgResized = cv2.resize(img, (args.image_size,args.image_size))
            #print(f'Image shape cv2.resize: {imgResized.shape}')

            """ cv2.imshow('Input image', imgResized)
            cv2.waitKey(0)
            cv2.destroyAllWindows() """
            
            seg_data = next(seg_loader)

            for j in range (0,args.batch):
                imgResizedT = seg_data['image'][j] #imgResized.transpose((2, 0, 1))[::-1]
                print(f'Image shape imgResizedT: {imgResizedT.shape}')

                """ plt.imshow(cv2.cvtColor(imgResized, cv2.COLOR_BGR2RGB))
                plt.title('Input resize image')
                plt.show()
                plt.close() """
                #sys.exit()
                
                sampleDict  = diffusion.p_sample_any_image(
                    model,
                    (args.batch_size, 3, args.image_size, args.image_size),
                    imgResizedT,
                    i_unet,
                    index,
                    #i_noise=i_noise,
                    #log_noise=log_noise,
                    #q_noise=q_noise,
                    noise=None,
                    clip_denoised=args.clip_denoised,
                    model_kwargs=model_kwargs,
                )
                
                #sys.exit()
                #sample = sampleNoise
                #print(f'Shape sample: {sample.size()}')
                #plt.imshow(sample.squeeze().permute(1, 2, 0))
                #plt.title('Ouput sample')
                #plt.show()
                #plt.close()

                #print(f'Type sample: {sampleDict.keys()}')
                sample = sampleDict['sample']
                intermediateLayers = sampleDict['intermediateLayers']
                #sys.exit()
                sample = ((sample + 1) * 127.5).clamp(0, 255).to(th.uint8)
                sample = sample.permute(0, 2, 3, 1)
                #sample = sample.squeeze(0).permute(1, 2, 0)
                #plt.imshow(sample.squeeze(0).permute(0, 1, 2))
                #plt.title("Sample result")
                #plt.show()
                
                intermediateLayersConcat = concatenateIntermediateLayers(args, intermediateLayers)
                intermediateLayers = th.from_numpy(np.concatenate([imgResizedT,intermediateLayersConcat],axis=0))
                all_intermediateLayers.append(intermediateLayers)

                #print(f'Shape saveLayers: {saveLayers.size()}')
                np.savez(f"/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/layers_{args.timestep_to_segment:04d}_image_{i}.npz", intermediateLayers)
                
                ##SEGMENTATION
                maskTensor = seg_data['mask'][j]
                all_masks.append(maskTensor)

                imageMaks = make_mask(8, maskTensor.unsqueeze(0), threshold=0.5)
                im = Image.fromarray(imageMaks[0],'RGB')
                im.save(f"/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/images/imageMaks_{i}_{j}_{timestr}.png")

                #fake_seg_img = make_mask(8, fake_seg.cpu(), threshold=0.5)
                #img_fake_seg = Image.fromarray(fake_seg_img[0],'RGB')
                #img_fake_seg.save(f"/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/images/fake_seg_img_{i}_{timestr}.png")

                #logger.log(f'Intermediate layers {i} genereted')
                all_images.extend([sample.cpu().numpy()])
                #i += 1

        logger.log(f'Start segmentation')
        pbar = range(args.iter)

        for idx in pbar:
            print(f'Iter:{idx}')
            train_v2(args, pbar, segGen, all_intermediateLayers, all_masks, g_optim, device)

            if idx % 100 == 0:
                #if get_rank() == 0 & args.distributed:
                #    print(f'Saved model, epoch: {idx}')
                #    path_save = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/model_saved_{timestr}.pt'
                #    th.save(segGen.state_dict(), path_save)
                

                #seg_mask_ce_img = make_mask(8, seg_mask_ce.unsqueeze(0).cpu(), threshold=0.5)
                #img_seg_mask_ce = Image.fromarray(seg_mask_ce_img[0],'RGB')
                #img_seg_mask_ce.save("/mnt/scratch/users/tic_163_uma/josdiafra/PhD/img_seg_mask_ce.png")

                #seg_mask_dice_img = make_mask(8, seg_mask_dice.cpu(), threshold=0.5)
                #img_seg_mask_dice = Image.fromarray(seg_mask_dice_img[0],'RGB')
                #img_seg_mask_dice.save("/mnt/scratch/users/tic_163_uma/josdiafra/PhD/img_seg_mask_dice.png")
                
                print(f'Saved model, epoch: {idx}')
                path_save = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/model_saved_{timestr}.pt'
                th.save(segGen.state_dict(), path_save)

        logger.log(f'End segmentation')
        logger.log(f"created {len(all_images) * args.batch_size} samples")

def concatenateIntermediateLayers(args, intermediateLayers: dict):
    del intermediateLayers['t']
    del intermediateLayers['index']

    upsampled_layers = []
    namesLayers = list(intermediateLayers.keys())
    #print(f'Num layers: {len(namesLayers)}')
    #print(f'Type intermediateLayers: {type(intermediateLayers)}')
    #print(f'Type names intermediateLayers: {type(namesLayers)}')
    for layer in namesLayers:
        #print(layer)
        #print(f'Size {layer}: {intermediateLayers[layer].shape}')
        if layer in (args.intermediate_layers_saved).split():
            multFactor = 256 / intermediateLayers[layer].squeeze().shape[2]
            #layer_upsampled = scipy.ndimage.zoom(intermediateLayers[layer].squeeze(), (1,multFactor,multFactor), order=1)
            layer_upsampled = scipy.ndimage.zoom(intermediateLayers[layer].squeeze()[0:256], (1,multFactor,multFactor), order=1)
            #print(f'Shape upsampled {layer_upsampled.shape}')
            #upsampled_layers.append(layer_upsampled.reshape(1,256,256))
            upsampled_layers.append(layer_upsampled)
            #print(f'Shape upsampled {layer_upsampled.shape}')

    layers_concatenated = np.concatenate(upsampled_layers, axis=0)
    print(f'Shape layers_concatenated {layers_concatenated.shape}')
    return layers_concatenated

def create_argparser():
    defaults = dict(
        clip_denoised=True,
        num_samples=4,
        batch_size=1,
        use_ddim=False,
        model_path="/mnt/home/users/tic_163_uma/josdiafra/PhD/diffusion-model-modified/models/model200000_celebA_256.pt",
        imagenet_class=235,
        modified = True,
        timestep_to_segment = 220,
        timestep_respacing = 250,
        image_to_segment = "",
        dataset = "",
        intermediate_layers_saved = "h_o14 h_o23", #"h_m h_o02 h_o07 h_o14 h_o23",
        path_dataset = "",
        save_results = "/mnt/scratch/users/tic_163_uma/josdiafra/PhD",
        ##Segmentation parameters
        img_dataset_path = '/mnt/scratch/users/tic_163_uma/josdiafra/datasets/celebA-HQ-256',
        #img_dataset = "",
        #seg_dataset = "",
        enc_backbone = "fpn",
        seg_name = "celeba-mask",
        batch = 2,
        limit_size = 4,
        n_gpu = 0,
        iter = 1000,
        lambda_label_ce = 1.0,
        lambda_label_dice = 1.0,
        g_reg_every = 4.0,
        lr = 0.002,
        local_rank = 0

    )
    defaults.update(model_and_diffusion_defaults())
    parser = argparse.ArgumentParser()
    add_dict_to_argparser(parser, defaults)
    return parser


if __name__ == "__main__":
    main()

"""
Generate a large batch of image samples from a model and save them as a large
numpy array. This can be used to produce samples for FID evaluation.
"""

import argparse
import os
import sys

import torch as th
import torch.distributed as dist

import cv2
import matplotlib.pyplot as plt

import scipy
import numpy as np


from guided_diffusion import dist_util, logger
from guided_diffusion.script_util import (
    NUM_CLASSES,
    model_and_diffusion_defaults,
    create_model_and_diffusion,
    add_dict_to_argparser,
    args_to_dict,
)


def main():
    args = create_argparser().parse_args()

    dist_util.setup_dist()
    logger.configure()

    logger.log("creating model and diffusion...")
    model, diffusion = create_model_and_diffusion(
        **args_to_dict(args, model_and_diffusion_defaults().keys())
    )
    #model.modelSummary()
    logger.log("model and diffusion created")
    model.load_state_dict(
        dist_util.load_state_dict(args.model_path, map_location="cpu")
    )
    logger.log("sending weights to GPU...")
    model.to(dist_util.dev())
    if args.use_fp16:
        model.convert_to_fp16()
    model.eval()

    datatsetImages = os.listdir(args.path_dataset)
    datatsetImages.sort()

    logger.log("sampling...")
    all_images = []
    all_labels = []
    i = 0

    while len(all_images) * args.batch_size < args.num_samples:
        model_kwargs = {}
        if args.class_cond:
            if args.dataset == 'imageNet-Dog':
                classes = th.from_numpy(np.array([args.imagenet_class], dtype=np.int64)).to(dist_util.dev())
                logger.log(f'Image class choosen: {classes}')
            else:
                classes = th.randint(
                    low=0, high=NUM_CLASSES, size=(args.batch_size,), device=dist_util.dev()
                )
            model_kwargs["y"] = classes
            sample_fn = (
                diffusion.p_sample_loop if not args.use_ddim else diffusion.ddim_sample_loop
            )
        if args.modified != True:
            sample = sample_fn(
                model,
                (args.batch_size, 3, args.image_size, args.image_size),
                clip_denoised=args.clip_denoised,
                model_kwargs=model_kwargs,
            )
        else:
            logger.log('Code modified')
            print(datatsetImages[i])
            args.image_to_segment = f'{args.path_dataset}/{datatsetImages[i]}'
            print(f'Path image: {datatsetImages[i]}')
            #diffusion.quitaruido = True
            #diffusion.always_save = True
            index = args.timestep_to_segment
            num_timesteps = int(args.timestep_respacing)
            i_noise=num_timesteps-index
            q_noise=num_timesteps-index
            print(f'noise: {num_timesteps}-{index} = {q_noise}')
            log_noise=-1.5
            i_unet=num_timesteps-index
            
            img = cv2.imread(args.image_to_segment)
            print(f'Image shape cv2.imread: {img.shape}')
            imgResized = cv2.resize(img, (args.image_size,args.image_size))
            print(f'Image shape cv2.resize: {imgResized.shape}')

            """ cv2.imshow('Input image', imgResized)
            cv2.waitKey(0)
            cv2.destroyAllWindows() """

            imgResizedT = imgResized.transpose((2, 0, 1))[::-1]
            print(f'Image shape imgResizedT: {imgResizedT.shape}')

            """ plt.imshow(cv2.cvtColor(imgResized, cv2.COLOR_BGR2RGB))
            plt.title('Input resize image')
            plt.show()
            plt.close() """
            #sys.exit()
            
            sampleDict  = diffusion.p_sample_any_image(
                model,
                (args.batch_size, 3, args.image_size, args.image_size),
                imgResizedT,
                i_unet,
                index,
                #i_noise=i_noise,
                #log_noise=log_noise,
                #q_noise=q_noise,
                noise=None,
                clip_denoised=args.clip_denoised,
                model_kwargs=model_kwargs,
            )
            
            #sys.exit()
            #sample = sampleNoise
            #print(f'Shape sample: {sample.size()}')
            #plt.imshow(sample.squeeze().permute(1, 2, 0))
            #plt.title('Ouput sample')
            #plt.show()
            #plt.close()

        #print(f'Type sample: {sampleDict.keys()}')
        sample = sampleDict['sample']
        intermediateLayers = sampleDict['intermediateLayers']
        #sys.exit()
        sample = ((sample + 1) * 127.5).clamp(0, 255).to(th.uint8)
        sample = sample.permute(0, 2, 3, 1)
        #sample = sample.squeeze(0).permute(1, 2, 0)
        #plt.imshow(sample.squeeze(0).permute(0, 1, 2))
        #plt.title("Sample result")
        #plt.show()
        imgSave = sample.squeeze(0).cpu().numpy()
        imgLayers = sample.squeeze(0).numpy().transpose(2,0,1)
        intermediateLayersConcat = concatenateIntermediateLayers(args, intermediateLayers)
        saveLayers = np.concatenate([imgLayers,intermediateLayersConcat],axis=0)
        #cv2.imwrite(f'/Users/joseangeldiazfrances/Documents/PhD/guided-diffusion-modified/resultsIntermediateLayers/result_image_celebA{i:04d}.png',cv2.cvtColor(imgSave,cv2.COLOR_BGR2RGB))
        #np.savez(f'/Users/joseangeldiazfrances/Documents/PhD/guided-diffusion-modified/resultsIntermediateLayers/intermediate_{args.timestep_to_segment:04d}.npz', **intermediateLayers)
        np.savez(f"{args.save_results}/layers_{args.timestep_to_segment:04d}_image_{(datatsetImages[i]).split('.')[0]}.npz", saveLayers)
        
        #sample = sample.contiguous()
        #plt.imshow(sample)
        #plt.show()
        all_images.extend([sample.cpu().numpy()])
        i += 1

        
    logger.log(f"created {len(all_images) * args.batch_size} samples")

    logger.log("sampling complete")

def concatenateIntermediateLayers(args, intermediateLayers: dict):
    del intermediateLayers['t']
    del intermediateLayers['index']

    upsampled_layers = []
    namesLayers = list(intermediateLayers.keys())
    #print(f'Num layers: {len(namesLayers)}')
    #print(f'Type intermediateLayers: {type(intermediateLayers)}')
    #print(f'Type names intermediateLayers: {type(namesLayers)}')
    for layer in namesLayers:
        #print(layer)
        #print(f'Size {layer}: {intermediateLayers[layer].shape}')
        if layer in (args.intermediate_layers_saved).split():
            multFactor = 256 / intermediateLayers[layer].squeeze().shape[2]
            #layer_upsampled = scipy.ndimage.zoom(intermediateLayers[layer].squeeze(), (1,multFactor,multFactor), order=1)
            layer_upsampled = scipy.ndimage.zoom(intermediateLayers[layer].squeeze()[0:4], (1,multFactor,multFactor), order=1)
            #print(f'Shape upsampled {layer_upsampled.shape}')
            #upsampled_layers.append(layer_upsampled.reshape(1,256,256))
            upsampled_layers.append(layer_upsampled)
            #print(f'Shape upsampled {layer_upsampled.shape}')

    layers_concatenated = np.concatenate(upsampled_layers, axis=0)
    print(f'Shape layers_concatenated {layers_concatenated.shape}')
    return layers_concatenated

def create_argparser():
    defaults = dict(
        clip_denoised=True,
        num_samples=50,
        batch_size=1,
        use_ddim=False,
        model_path="",
        imagenet_class=235,
        modified = True,
        timestep_to_segment = 220,
        timestep_respacing = 250,
        image_to_segment = "",
        dataset = "",
        intermediate_layers_saved = "h_m h_o02 h_o07 h_o14 h_o23",
        path_dataset = "",
        save_results = ""
    )
    defaults.update(model_and_diffusion_defaults())
    parser = argparse.ArgumentParser()
    add_dict_to_argparser(parser, defaults)
    return parser


if __name__ == "__main__":
    main()

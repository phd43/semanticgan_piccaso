import torch as th
from torchvision import transforms as T
from torch.utils.data import Dataset
from torch.utils import data
from torch import optim
from torchvision import utils
from torch.utils.tensorboard import SummaryWriter
import torchvision

import albumentations
import albumentations.augmentations as A


import argparse
#from asyncio.log import logger
import inspect
from PIL import Image
import numpy as np
import os
import time
import sys

from guided_diffusion import gaussian_diffusion as gd
from guided_diffusion.respace import SpacedDiffusion, space_timesteps
from guided_diffusion.unet import *
from guided_diffusion.dataloader.dataset import CelebAMaskDataset
from guided_diffusion import dist_util, logger
from guided_diffusion.image_datasets import load_data

from guided_diffusion.train_util import TrainLoop

from guided_diffusion.modelsSemantic.stylegan2_seg import ToSEG, ConstantInput, StyledConv #,GeneratorSeg
from guided_diffusion.modelsSemantic.utils import *
from guided_diffusion.modelsSemantic.encoder_model import FPNEncoder
from guided_diffusion.losses import SoftmaxLoss, DiceLoss, FocalLoss, JaccardIndex
#from dataloader.dataset import CelebAMaskDataset
from guided_diffusion.modelsSemantic.lpips import PerceptualLoss

from guided_diffusion.segmentation import GeneratorSeg
from guided_diffusion.script_util import (
    model_and_diffusion_defaults,
    create_model_and_diffusion,
    args_to_dict,
    add_dict_to_argparser,
    create_gaussian_diffusion
)
#from image_segmentation import *

device = 'cpu' if th.cuda.is_available() != True else 'cuda'
print(f'Device: {device}')
dist_util.setup_dist()

timestr = time.strftime("%Y%m%d-%H%M%S")
writer = SummaryWriter(f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/tensorboard_{timestr}_imgs_lr_002_batch_4_optimizer__modelSemi_outputLayers_concatenated_fullDataset_mIoU_proof')

image_size = 256
num_channels = 256
num_res_blocks = 3
channel_mult = ""
learn_sigma = True
class_cond= False 
use_checkpoint = False
attention_resolutions = "32,16,8"
num_heads = 4
num_head_channels = 64
num_heads_upsample = -1
use_scale_shift_norm = True
dropout = 0.3
resblock_updown = False
use_fp16 = False
use_new_attention_order = False
size=256
style_dim=256
n_mlp=8
inChannels=6
outChannels=1
activationRelu=False

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--img_dataset', type=str)
parser.add_argument('--img_dataset_path', type=str)
parser.add_argument('--seg_dataset', type=str)
parser.add_argument('--enc_backbone', type=str, default='fpn')
parser.add_argument('--seg_name', type=str, default='celeba-mask')
parser.add_argument('--batch', type=int, default=4)
parser.add_argument('--n_gpu', type=int, default=0)
parser.add_argument('--iter', type=int, default=60000)
parser.add_argument('--lr_decay', type=float, default=0.00001)
parser.add_argument('--lr_decay_iter_start', type=int, default=3000)#30000)
parser.add_argument('--lr_decay_iter_end', type=int, default=100000)
parser.add_argument('--lambda_label_ce', type=float, default=1.0)
parser.add_argument('--lambda_label_dice', type=float, default=1.0)
parser.add_argument('--lambda_label_mse', type=float, default=1.0)
parser.add_argument('--g_reg_every', type=int, default=4)
parser.add_argument('--lr', type=float, default=0.002)

img_dataset_path = '/mnt/scratch/users/tic_163_uma/josdiafra/datasets/CelebA-HQ_256_list2' #'/mnt/scratch/users/tic_163_uma/josdiafra/datasets/CelebA-HQ-256-final-filtered'
limit_data = None

args = parser.parse_args([])


#utils
def update_learning_rate(args, i, optimizer):
    if i < args.lr_decay_iter_start:
        pass
    elif i < args.lr_decay_iter_end:
        lr_max = args.lr
        lr_min = args.lr_decay
        t_max = args.lr_decay_iter_end - args.lr_decay_iter_start
        t_cur = i - args.lr_decay_iter_start

        optimizer.param_groups[0]['lr'] = lr_min + 0.5 * (lr_max - lr_min) * (
                    1 + math.cos(t_cur * 1.0 / t_max * math.pi))
    else:
        pass

def get_transformation(args):
    if args.seg_name == 'celeba-mask':
        transform = T.Compose(
            [
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True)
            ]
        )
    else:
        raise Exception('No such a dataloader!')

    return transform

convert_tensor = T.ToTensor()

def data_sampler(dataset, shuffle, distributed):
    if distributed:
        return data.distributed.DistributedSampler(dataset, shuffle=shuffle)

    if shuffle:
        return data.RandomSampler(dataset)

    else:
        return data.SequentialSampler(dataset)

def sample_data(loader):
    while True:
        for batch in loader:
            yield batch

def mask2rgb(seg_name, mask):
    if seg_name == 'celeba-mask':
        color_table = th.tensor(
            [[0, 0, 0],
             [0, 0, 205],
             [132, 112, 255],
             [25, 25, 112],
             [187, 255, 255],
             [102, 205, 170],
             [227, 207, 87],
             [142, 142, 56]
             ], dtype=th.float)
    else:
        raise Exception('No such a dataloader!')

    rgb_tensor = F.embedding(mask, color_table).permute(0, 3, 1, 2)
    return rgb_tensor

def make_mask(seg_dim, tensor, threshold=0.5):
    if seg_dim == 1:
        seg_prob = th.sigmoid(tensor)
        seg_mask = th.zeros_like(tensor)
        seg_mask[seg_prob > threshold] = 1.0
        seg_mask = (seg_mask.to('cpu')
                       .mul(255)
                       .type(th.uint8)
                       .permute(0, 2, 3, 1)
                       .numpy())
    else:
        seg_prob = th.argmax(tensor, dim=1)
        seg_mask = mask2rgb('celeba-mask', seg_prob)
        seg_mask = (seg_mask.to('cpu')
                       .type(th.uint8)
                       .permute(0, 2, 3, 1)
                       .numpy())
    return seg_mask

def mask2class(seg_name, mask):
    if seg_name == 'celeba-mask':
        color_table = th.tensor(
            [[0],
             [1],
             [2],
             [3],
             [4],
             [5],
             [6],
             [7]
             ], dtype=th.float)
    else:
        raise Exception('No such a dataloader!')

    rgb_tensor = F.embedding(mask, color_table).permute(0, 3, 1, 2)
    return rgb_tensor

def make_mask_classes(seg_dim, tensor, threshold=0.5):
    if seg_dim == 1:
        seg_prob = th.sigmoid(tensor)
        seg_mask = th.zeros_like(tensor)
        seg_mask[seg_prob > threshold] = 1.0
        seg_mask = (seg_mask.to('cpu')
                       .mul(255)
                       .type(th.uint8)
                       .permute(0, 2, 3, 1)
                       .numpy())
    else:
        seg_prob = th.argmax(tensor, dim=1)
        seg_mask = mask2class('celeba-mask', seg_prob)
        seg_mask = (seg_mask.to('cpu')
                       .type(th.uint8)
                       #.permute(0, 2, 3, 1)
                       .numpy())
    

    return seg_mask

def save_results(outputResults):
    np.savez(f"/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/final_results.npz", outputResults)
    logger.log(f'Results saved!')

def create_model(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelModified(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )

def create_model_semi(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelSemi(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )
def create_model_semi_baseline(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelSemiBaseline(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )

def create_model_semi_upsample(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelModified_upsample(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )

#Upsample concatenate UNetModelModified_upsample_concatenate
def create_model_semi_concatenate_outputLayers(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelModified_concatenate_output(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )

def create_model_semi_outputLayers_reshape(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelModified_output_reshape(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )

#Upsameple concatenate UNetModelModified_upsample_concatenate
def create_model_semi_upsample_concatenate(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelModified_upsample_concatenate(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )

#UnetSemiWithoutTimeEmbedding
def create_model_semi_withoutTimeEmbedding(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelSemiWithoutTimeEmbedding(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=(NUM_CLASSES if class_cond else None),
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
    )


def create_model_semi_segmentation(
    image_size,
    num_channels,
    num_res_blocks,
    channel_mult="",
    learn_sigma=False,
    class_cond=False,
    use_checkpoint=False,
    attention_resolutions="16",
    size=256,
    style_dim=256,
    n_mlp=8,
    inChannels=256,
    outChannels=1,
    activationRelu=False,
    num_heads=1,
    num_head_channels=-1,
    num_heads_upsample=-1,
    use_scale_shift_norm=False,
    dropout=0,
    resblock_updown=False,
    use_fp16=False,
    use_new_attention_order=False,
):
    if channel_mult == "":
        if image_size == 512:
            channel_mult = (0.5, 1, 1, 2, 2, 4, 4)
        elif image_size == 256:
            channel_mult = (1, 1, 2, 2, 4, 4)
        elif image_size == 128:
            channel_mult = (1, 1, 2, 3, 4)
        elif image_size == 64:
            channel_mult = (1, 2, 3, 4)
        else:
            raise ValueError(f"unsupported image size: {image_size}")
    else:
        channel_mult = tuple(int(ch_mult) for ch_mult in channel_mult.split(","))

    attention_ds = []
    for res in attention_resolutions.split(","):
        attention_ds.append(image_size // int(res))

    return UNetModelSemiSegmentation(
        image_size=image_size,
        in_channels=3,
        model_channels=num_channels,
        out_channels=(3 if not learn_sigma else 6),
        num_res_blocks=num_res_blocks,
        attention_resolutions=tuple(attention_ds),
        dropout=dropout,
        channel_mult=channel_mult,
        num_classes=None,
        use_checkpoint=use_checkpoint,
        use_fp16=use_fp16,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        resblock_updown=resblock_updown,
        use_new_attention_order=use_new_attention_order,
        size=size,
        style_dim=style_dim,
        n_mlp=n_mlp,
        inChannels=inChannels,
        outChannels=outChannels,
        activationRelu=activationRelu,
    )

model = create_model(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

model_semi = create_model_semi(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

model_semi_baseline = create_model_semi_baseline(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)
model_semi_upsample = create_model_semi_upsample(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

model_semi_upsample_concatenate = create_model_semi_upsample_concatenate(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

model_semi_concatenate_outputLayers = create_model_semi_concatenate_outputLayers(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

model_semi_outputLayers_reshape = create_model_semi_outputLayers_reshape(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

#Model semi without TimeEmbedding
model_semi_withoutTimeEmbedding = create_model_semi_withoutTimeEmbedding(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
)

model_semi_segmentation = create_model_semi_segmentation(
        image_size,
        num_channels,
        num_res_blocks,
        channel_mult=channel_mult,
        learn_sigma=learn_sigma,
        class_cond=class_cond,
        use_checkpoint=use_checkpoint,
        attention_resolutions=attention_resolutions,
        num_heads=num_heads,
        num_head_channels=num_head_channels,
        num_heads_upsample=num_heads_upsample,
        use_scale_shift_norm=use_scale_shift_norm,
        dropout=dropout,
        resblock_updown=resblock_updown,
        use_fp16=use_fp16,
        use_new_attention_order=use_new_attention_order,
        size=size,
        style_dim=style_dim,
        n_mlp=n_mlp,
        inChannels=inChannels,
        outChannels=outChannels,
        activationRelu=activationRelu,
)

model_path = "/mnt/scratch/users/tic_163_uma/josdiafra/PhD/guided-diffusion/results/modelCifarUnconditionalCelebA_256_v2_lr_1e5/model200000.pt"
model.load_state_dict(th.load(model_path, map_location="cpu"))
#model_semi.load_state_dict(th.load(model_path, map_location="cpu"))
#model_semi_baseline.load_state_dict(th.load(model_path, map_location="cpu"))
model_segmentation = model_semi_concatenate_outputLayers #model_semi_outputLayers_reshape
model_segmentation.load_state_dict(th.load(model_path, map_location="cpu"))

model.to(device)
#model_semi.to(device)
#model_semi_baseline.to(device)
model_segmentation.to(device)

g_reg_every = args.g_reg_every
g_reg_ratio = g_reg_every / (g_reg_every + 1)
iter = args.iter
start_iter = 0
pbar = range(args.iter)
lr = args.lr
lambda_label_ce = args.lambda_label_ce
lambda_label_dice = args.lambda_label_dice
lambda_label_mse = args.lambda_label_mse

channels = 256
seg_dim = 8#3
style_dim = 256
size = 256
n_mlp = 8
channel_multiplier = 2
inChannels = 6
outChannels = 1

transform = get_transformation(args)

img_dataset = CelebAMaskDataset(args, img_dataset_path, unlabel_transform=transform,
                                            limit_size=limit_data,
                                            is_label=True, phase='train',resolution=size)

img_dataset_validation = CelebAMaskDataset(args, img_dataset_path, unlabel_transform=transform,
                                            limit_size=limit_data,
                                            is_label=True, phase='val',resolution=size)

img_loader = data.DataLoader(
        img_dataset,
        batch_size=args.batch,
        sampler=data_sampler(img_dataset, shuffle=True, distributed=False),
        drop_last=True,
        pin_memory=True,
        num_workers=4,
    )

img_loader_validation = data.DataLoader(
        img_dataset_validation,
        batch_size=args.batch,
        sampler=data_sampler(img_dataset_validation, shuffle=False, distributed=False),
        drop_last=True,
        pin_memory=True,
        num_workers=4,
    )

seg_loader = sample_data(img_loader)
seg_loader_validation = sample_data(img_loader_validation)

data = load_data(
        data_dir= "/mnt/scratch/users/tic_163_uma/josdiafra/datasets/CelebA-HQ_256_list2/label_data/image", #"/mnt/scratch/users/tic_163_uma/josdiafra/datasets/CelebA-HQ-256-final-filtered/label_data/image"
        batch_size=1,
        image_size=256,
        class_cond=False,
    )

def requires_grad(model, flag=True):
    for p in model.parameters():
        p.requires_grad = flag

ce_loss_func = SoftmaxLoss(tau=0.1)
dice_loss_func = DiceLoss(sigmoid_tau=0.3)
focal_loss_func = FocalLoss(alpha=0.25, gamma=2, reduction='mean')

segGen = GeneratorSeg(size,style_dim,n_mlp,inChannels,outChannels,False,seg_dim=seg_dim,channel_multiplier=channel_multiplier).to(device)

diffusion = create_gaussian_diffusion(
            steps=1000,
            learn_sigma=learn_sigma,
            noise_schedule="linear",
            use_kl=False,
            predict_xstart=False,
            rescale_timesteps=False,
            rescale_learned_sigmas=False,
            timestep_respacing="",
        )

g_optim_seg = optim.Adam(
        segGen.parameters(),
        lr=lr * g_reg_ratio,
        betas=(0 ** g_reg_ratio, 0.99 ** g_reg_ratio),
    )

#g_optim_unet = optim.AdamW(
#        model_semi.parameters(),
#        lr=1e-4,
#        weight_decay=0.0,
#    )

g_optim_unet = optim.AdamW(
        model_segmentation.parameters(),
        lr=1e-4,
        weight_decay=0.0,
    )

requires_grad(segGen, True)
#requires_grad(model_semi, False)
requires_grad(model_segmentation, False)
requires_grad(model,True)

#output_model_semi, intermediateLayers_model_semi = model_semi(intermediateLayers_list_copy,intermediateLastLayers_list_inverse_copy,timestep)
timestep = th.Tensor([220]).to(device)

logger.log('Start trainning')

def printIntermediateLayers(intermediateLastLayers_dict):
    for k, v in intermediateLastLayers_dict.items():
        print(f'{k}:{v.shape}')

for idx in pbar:
    loss_values = np.array([])
    array_mask_pred = []
    array_mask = []
    i = idx + start_iter
    
    if i > iter:
        print('Done!')
        break

    seg_data = next(seg_loader)

    for j in range(0,img_loader.batch_size):
        imageTensor = seg_data['image_segmentation'][j].unsqueeze(0).to(device)

        #output, intermediateLayers_dict, intermediateLastLayers_dict = model(imageTensor,timestep)
        output, inputLayers_dict, outputLayers_dict = model(imageTensor,timestep)

        outputLayersReshape = outputLayers_dict #reshapeLayers(outputLayers_dict)

        #intermediateLayers_list = list(intermediateLayers.values())

        inputLayers_list = list(inputLayers_dict.values())

        outputLayers_list_inverse = []
        outputLayers_list = list(outputLayersReshape.values())

        for h in range(0,len(outputLayers_list)):
            outputLayers_list_inverse.append(outputLayers_list.pop())
        
        outputLayers_list_inverse_copy = outputLayers_list_inverse.copy()
        intermediateLayer = outputLayers_list_inverse_copy.pop()

        maskTensor = seg_data['mask'][j].unsqueeze(0)

        output_model_semi = model_segmentation(inputLayers_list, intermediateLayer, outputLayers_list_inverse_copy, timestep)
        #output_model_semi, intermediateLayers_out = model_segmentation(outputLayers_list, timestep)
        #output_model_semi = model_semi_baseline(intermediateLayers_list_copy, timestep)
        
        #print(output_model_semi.size())
        fake_seg = segGen(output_model_semi)
        
        #fake_seg, output_image, intermediateLayers_model = model_semi_segmentation(intermediateLayers_list_copy,intermediateLastLayers_list_inverse_copy,timestep)
        #print(f'Shape fake_seg: {fake_seg.size()}')
        #print(f'Shape maskTensor: {maskTensor.size()}')
        #fake_seg_mean = th.mean(fake_seg, dim=0).type(th.float).unsqueeze(0)

        seg_mask_ce = th.argmax(maskTensor, dim=1)
        seg_mask_dice = (maskTensor + 1.0) / 2.0

        #print(f'Shape seg_mask_ce: {seg_mask_ce.size()}')
        #print(f'Shape seg_mask_dice: {seg_mask_dice.size()}')
        #print(f'Shape fake_seg: {fake_seg.size()}')

        g_label_ce_loss = ce_loss_func(fake_seg, seg_mask_ce.to(device))
        g_label_dice_loss = dice_loss_func(fake_seg, seg_mask_dice.to(device))
        #g_focal_loss = focal_loss_func.getFocalLoss(fake_seg, seg_mask_dice.to(device))

        g_label_loss = (g_label_ce_loss * lambda_label_ce +
                        g_label_dice_loss * lambda_label_dice
                        #g_focal_loss
                        )

        segGen.zero_grad()
        model_segmentation.zero_grad()
        g_label_loss.backward()
        g_optim_seg.step()
        
        update_learning_rate(args,i,g_optim_seg)
        g_optim_unet.step()

        TrainLoop(
            model=model,
            diffusion=diffusion,
            data=data,
            batch_size=1,
            microbatch=-1,
            lr=1e-4,
            ema_rate=0.9999,
            log_interval=10,
            save_interval=1000,
            resume_checkpoint=None,
            use_fp16=False,
            fp16_scale_growth=1e-3,
            schedule_sampler=None,
            weight_decay=0.0,
            lr_anneal_steps=1,
            writter=None
        ).run_loop_direct_input(seg_data['image_unet'][j].unsqueeze(0).to(device))

        logger.log(f'Iter:{i}, Image:{j}, Loss image: {g_label_loss}')
        loss_values = np.append(loss_values,g_label_loss.detach().cpu().numpy())

        #Save images
        fake_seg_eval_cpu = fake_seg.detach().cpu()
        mask_gen_pred = make_mask(8, fake_seg_eval_cpu).squeeze() #make_mask(8, fake_seg_eval_cpu).squeeze()
        mask_gen_pred_tensor = convert_tensor(mask_gen_pred)

        mask_original = make_mask(8, maskTensor).squeeze() #make_mask(6, fake_seg_eval_cpu).squeeze()
        mask_original_tensor = convert_tensor(mask_original)

        array_mask_pred.append(mask_gen_pred_tensor.unsqueeze(0))
        array_mask.append(mask_original_tensor.unsqueeze(0))
    
    writer.add_scalar('Loss image',np.mean(loss_values),i)
    
    if i % 100 == 0:
        seg_data_validation = next(seg_loader_validation)
        jaccardIndexArray = np.array([])

        with th.no_grad():
            for h in range(0,img_loader.batch_size):

                imageTensor = seg_data['image_segmentation'][h].unsqueeze(0).to(device)

                output, inputLayers_dict, outputLayers_dict = model(imageTensor,timestep)

                outputLayersReshape = outputLayers_dict #reshapeLayers(outputLayers_dict)

                inputLayers_list = list(inputLayers_dict.values())

                outputLayers_list_inverse = []
                outputLayers_list = list(outputLayersReshape.values())

                for idx in range(0,len(outputLayers_list)):
                    outputLayers_list_inverse.append(outputLayers_list.pop())
                
                outputLayers_list_inverse_copy = outputLayers_list_inverse.copy()
                intermediateLayer = outputLayers_list_inverse_copy.pop()

                maskTensor = seg_data['mask'][h].unsqueeze(0)

                output_model_semi = model_segmentation(inputLayers_list, intermediateLayer, outputLayers_list_inverse_copy, timestep)
                fake_seg = segGen(output_model_semi)

                fake_seg_eval_cpu = fake_seg.detach().cpu()
                mask_gen_pred = make_mask_classes(8, fake_seg_eval_cpu).squeeze()
                mask_gen = make_mask_classes(8, maskTensor).squeeze()
            
                jaccardIndex = JaccardIndex(target=th.from_numpy(mask_gen), pred=th.from_numpy(mask_gen_pred))
                jaccardIndexArray = np.append(jaccardIndexArray,jaccardIndex)

        logger.log(f'Iter:{i}, Jaccard metric: {np.mean(jaccardIndexArray)}')
        writer.add_scalar('mIoU',np.mean(jaccardIndexArray),i)

        tensor_list_mask_pred = th.cat(array_mask_pred,0)
        tensor_list_mask_original = th.cat(array_mask,0)

        utils.save_image(
            tensor_list_mask_pred,
            os.path.join(f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/imgs_lr_002_batch_4_optimizer_modelSemi_outputLayers_concatenated_fullDataset_mIoU_proof/',f'mask_pred_{str(i).zfill(6)}.png'),
            nrow=int(args.batch ** 0.5)
        )

        utils.save_image(
            tensor_list_mask_original,
            os.path.join(f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/imgs_lr_002_batch_4_optimizer_modelSemi_outputLayers_concatenated_fullDataset_mIoU_proof/',f'mask_original_{str(i).zfill(6)}.png'),
            nrow=int(args.batch ** 0.5)
        )
    
    if i % 10000 == 0:
        path_save_unet_img = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/model_image_unet_saved_{timestr}_{i}_modelSemi_outputLayers_concatenated_fullDataset_mIoU_proof.pt'
        th.save(model.state_dict(), path_save_unet_img)

        path_save_unet_mask = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/model_mask_unet_saved_{timestr}_{i}_modelSemi_outputLayers_concatenated_fullDataset_mIoU_proof.pt'
        th.save(model_segmentation.state_dict(), path_save_unet_mask)

        path_save_seg = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet/model_segmentation_saved_{timestr}_{i}_modelSemi_outputLayers_concatenated_fullDataset_mIoU_proof.pt'
        th.save(segGen.state_dict(), path_save_seg)


#save_results(fake_seg.detach().cpu())
#Save model


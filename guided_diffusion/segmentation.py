import torch as th
from torch import nn
from torch.nn import functional as F
import torch.nn.utils.spectral_norm as spectral_norm
import numpy as np
import random
import torchvision
import torchvision.transforms as T
from torchvision import utils
from torch._C import dtype
from torch.utils import data
from torch import optim
from torch import distributed as dist


import matplotlib.pyplot as plt
from PIL import Image
import argparse

import os
import math
import time


from guided_diffusion.modelsSemantic.stylegan2_seg import GeneratorSeg, ToSEG, ConstantInput, StyledConv
from guided_diffusion.modelsSemantic.utils import *
from guided_diffusion.modelsSemantic.encoder_model import FPNEncoder
from guided_diffusion.dataloader.dataset import CelebAMaskDataset
from guided_diffusion.modelsSemantic.lpips import PerceptualLoss
from guided_diffusion.utils.distributed import (
    get_rank,
    synchronize,
    reduce_loss_dict,
)

class GeneratorSeg(nn.Module):
    def __init__(
            self,
            size,
            style_dim,
            n_mlp,
            inChannels,
            outChannels,
            activationRelu,
            seg_dim=1,
            image_mode='L',
            channel_multiplier=2,
            blur_kernel=[1, 3, 3, 1],
            lr_mlp=0.01,
    ):
        super().__init__()

        self.size = size

        self.style_dim = style_dim

        self.seg_dim = seg_dim

        self.convPoolingChannel = th.nn.Conv2d(in_channels=inChannels,out_channels=outChannels,kernel_size=1)
        self.relu = th.nn.ReLU()
        self.activation = activationRelu

        layers = [PixelNorm()]

        for i in range(n_mlp):
            layers.append(
                EqualLinear(
                    style_dim, style_dim, lr_mul=lr_mlp, activation='fused_lrelu'
                )
            )

        self.style = nn.Sequential(*layers)

        self.channels = {
            4: 512,
            8: 512,
            16: 512,
            32: 512,
            64: 256 * channel_multiplier,
            128: 128 * channel_multiplier,
            256: 64 * channel_multiplier,
            512: 32 * channel_multiplier,
            1024: 16 * channel_multiplier,
        }

        self.input = ConstantInput(self.channels[4])
        self.conv1 = StyledConv(
            self.channels[4], self.channels[4], 3, style_dim, blur_kernel=blur_kernel
        )
        if image_mode == 'RGB':
            self.rgb_channel = 3
        else:
            self.rgb_channel = 1

        self.to_seg1 = ToSEG(self.channels[4], self.seg_dim, style_dim, upsample=False)

        self.log_size = int(math.log(size, 2))
        self.num_layers = (self.log_size - 2) * 2 + 1

        self.convs = nn.ModuleList()
        self.upsamples = nn.ModuleList()
        self.to_rgbs = nn.ModuleList()
        self.to_segs = nn.ModuleList()
        self.noises = nn.Module()

        in_channel = self.channels[4]

        for layer_idx in range(self.num_layers):
            res = (layer_idx + 5) // 2
            shape = [1, 1, 2 ** res, 2 ** res]
            self.noises.register_buffer(f'noise_{layer_idx}', th.randn(*shape))

        for i in range(3, self.log_size + 1):
            out_channel = self.channels[2 ** i]
            # print(i, in_channel, out_channel)
            self.convs.append(
                StyledConv(
                    in_channel,
                    out_channel,
                    3,
                    style_dim,
                    upsample=True,
                    blur_kernel=blur_kernel,
                )
            )

            self.convs.append(
                StyledConv(
                    out_channel, out_channel, 3, style_dim, blur_kernel=blur_kernel
                )
            )

            self.to_segs.append(ToSEG(out_channel, self.seg_dim, style_dim))

            in_channel = out_channel

        self.n_latent = self.log_size * 2 - 2

    def make_noise(self):
      #device = self.input.input.device

      noises = [th.randn(1, 1, 2 ** 2, 2 ** 2, device=device)]

      for i in range(3, self.log_size + 1):
          for _ in range(2):
              noises.append(th.randn(1, 1, 2 ** i, 2 ** i, device=device))

      return noises

    def mean_latent(self, n_latent):
        latent_in = th.randn(
            n_latent, self.style_dim, device=self.input.input.device
        )
        latent = self.style(latent_in).mean(0, keepdim=True)

        return latent

    def get_latent(self, input):
        return self.style(input)

    def sample(self, num, latent_space_type='Z'):
        """Samples latent codes randomly.
        Args:
        num: Number of latent codes to sample. Should be positive.
        latent_space_type: Type of latent space from which to sample latent code.
            Only [`Z`, `W`, `WP`] are supported. Case insensitive. (default: `Z`)
        Returns:
        A `numpy.ndarray` as sampled latend codes.
        Raises:
        ValueError: If the given `latent_space_type` is not supported.
        """
        latent_space_type = latent_space_type.upper()
        if latent_space_type == 'Z':
            latent_codes = np.random.randn(num, self.style_dim)
        elif latent_space_type == 'W':
            latent_codes = np.random.randn(num, self.style_dim)
        elif latent_space_type == 'WP':
            latent_codes = np.random.randn(num, self.n_latent, self.style_dim)
        else:
            raise ValueError(f'Latent space type `{latent_space_type}` is invalid!')

        return latent_codes.astype(np.float32)

    def forward(
            self,
            styles,
            return_latents=False,
            return_latent_output=False,
            inject_index=None,
            truncation=1,
            truncation_latent=None,
            input_is_latent=False,
            noise=None,
            randomize_noise=True,
            return_mimaps=False,
    ):
        styles = self.convPoolingChannel(styles)
        #print(f'Size styles: {styles.size()}')
        
        if self.activation:
            styles = self.relu(styles)

        if not input_is_latent:
            styles = [self.style(s) for s in styles]
            #print(f'Length styles: {len(styles}')

        if noise is None:
            if randomize_noise:
                noise = [None] * self.num_layers
            else:
                noise = [
                    getattr(self.noises, f'noise_{i}') for i in range(self.num_layers)
                ]

        if truncation < 1:
            style_t = []

            for style in styles:
                style_t.append(
                    truncation_latent + truncation * (style - truncation_latent)
                )

            styles = style_t

        if len(styles) < 2:
            inject_index = self.n_latent

            if styles[0].ndim < 3:
                latent = styles[0].unsqueeze(1).repeat(1, inject_index, 1)

            else:
                latent = styles[0]

        else:
            if inject_index is None:
                inject_index = random.randint(1, self.n_latent - 1)

            latent = styles[0].unsqueeze(1).repeat(1, inject_index, 1)
            latent2 = styles[1].unsqueeze(1).repeat(1, self.n_latent - inject_index, 1)

            latent = th.cat([latent, latent2], 1)
        
        #latent = th.mean(latent, dim=0).unsqueeze(0)
        #print(f'Shape latent: {latent.size()}')
        out = self.input(latent)

        out = self.conv1(out, latent[:, 0], noise=noise[0])

        #skip_rgb = self.to_rgb1(out, latent[:, 1]) #Genera la imagen
        skip_seg = self.to_seg1(out, latent[:, 1]) #Genera la segmentacion

        #print(f'Shape skip_seg {skip_seg.size()}')

        if return_mimaps:
            #image_mimaps = []
            label_mimaps = []
            #skip_rgb_norm = skip_rgb.detach().cpu()
            #skip_rgb_norm = (skip_rgb_norm - skip_rgb_norm.min()) / (skip_rgb_norm.max() - skip_rgb_norm.min())
            #skip_rgb_norm = skip_rgb_norm[0].permute(1, 2, 0)

            skip_seg_norm = skip_seg.detach().cpu()
            skip_seg_norm = (skip_seg_norm - skip_seg_norm.min()) / (skip_seg_norm.max() - skip_seg_norm.min())
            skip_seg_norm = skip_seg_norm[0].permute(1, 2, 0)

            #image_mimaps.append(skip_rgb_norm)
            label_mimaps.append(skip_seg_norm)

        i = 1
        #print(f'Length self.convs[::2]: {len(self.convs[::2])}')
        #print(f'Length self.convs[1::2]: {len(self.convs[1::2])}')
        #print(f'Length self.to_rgbs: {len(self.to_rgbs)}')
        
        for conv1, conv2, noise1, noise2, to_seg in zip(
                self.convs[::2], self.convs[1::2], noise[1::2], noise[2::2], self.to_segs
        ):
            out = conv1(out, latent[:, i], noise=noise1)

            out = conv2(out, latent[:, i + 1], noise=noise2)

            #skip_rgb = to_rgb(out, latent[:, i + 2], skip_rgb)

            skip_seg = to_seg(out, latent[:, i + 2], skip_seg)
            #print(f'Shape skip_seg {skip_seg.size()}')
            #print(f'Shape latent[:, i + 2] {latent[:, i + 2].size()}')
            #print(f'Shape latent {latent.size()}')

            if return_mimaps:
                #skip_rgb_norm = skip_rgb.detach().cpu()
                #skip_rgb_norm = (skip_rgb_norm - skip_rgb_norm.min()) / (skip_rgb_norm.max() - skip_rgb_norm.min())
                #skip_rgb_norm = skip_rgb_norm[0].permute(1, 2, 0)

                skip_seg_norm = skip_seg.detach().cpu()
                skip_seg_norm = (skip_seg_norm - skip_seg_norm.min()) / (skip_seg_norm.max() - skip_seg_norm.min())
                skip_seg_norm = skip_seg_norm[0].permute(1, 2, 0)

                #image_mimaps.append(skip_rgb_norm)
                label_mimaps.append(skip_seg_norm)

            i += 2

        #image = skip_rgb
        seg = skip_seg
        #print(f'Shape seg {seg.size()}')
        if return_latents:
            return latent#image, latent
        elif return_latent_output:
            return latent#image, seg, latent
        elif return_mimaps:
            return latent#image_mimaps, label_mimaps
        else:
            return seg#image, seg



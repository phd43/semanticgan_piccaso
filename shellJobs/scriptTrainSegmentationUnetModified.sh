#!/usr/bin/env bash
# Leave only one comment symbol on selected options
# Those with two commets will be ignored:
# The name to show in queue lists for this job:
#SBATCH -J train_semanticGAN_segmentation_modified_unet.sh

# Number of desired cpus (can be in any node):
#SBATCH --ntasks=4

# Number of desired cpus (all in same node):
#SBATCH --cpus-per-task=8
#SBATCH --ntasks-per-node=4

# Amount of RAM needed for this job:
#SBATCH --mem=40gb

# The time the job will be running:
#SBATCH --time=96:00:00

# To use GPUs you have to request them:
#SBATCH --gres=gpu:1
#SBATCH --constraint=dgx

##SBATCH --constraint=cal

# Set output and error files
#SBATCH --error=train_semanticGAN_segmentation_modified_unet.%J.err
#SBATCH --output=train_semanticGAN_segmentation_modified_unet.%J.out

# To load some software (you can show the list with 'module avail'):

module load miniconda/3
source activate /mnt/scratch/users/tic_163_uma/josdiafra/condaEnvironments/deepLearning3.8

export OPENAI_LOGDIR="/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet"
export PYTHONPATH="/mnt/home/users/tic_163_uma/josdiafra/PhD_git/semanticgan_piccaso"
export CUDA_VISIBLE_DEVICES=0 #,1
export PYTORCH_CUDA_ALLOC_CONF=max_split_size_mb:32


# the program to execute with its parameters:
##echo time python curso_ok.py --epochs 3 --batch-size 512
nvidia-smi
echo "GIT"
echo "Init job (SemanticGAN): train segmentation unet modified"


time /mnt/scratch/users/tic_163_uma/josdiafra/condaEnvironments/deepLearning3.8/bin/python3.8 /mnt/home/users/tic_163_uma/josdiafra/PhD_git/semanticgan_piccaso/scripts/image_segmentation_draft.py
#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/improved-diffursion"
export PYTHONPATH="/home/joseangel/Documents/improved-diffusion-main"

python /home/joseangel/Documents/improved-diffusion-main/scripts/image_sample.py --model_path /home/joseangel/Documents/improved-diffusion-main/models/cifar10_uncond_50M_500K.pt --image_size 32 --num_channels 128 --num_res_blocks 3 --learn_sigma True --dropout 0.3 --diffusion_steps 4000 --noise_schedule cosine


echo "PID of this script: $$"

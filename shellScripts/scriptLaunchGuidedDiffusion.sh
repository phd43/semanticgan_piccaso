#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffursion-LSUN/samples"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

python /home/joseangel/Documents/guided-diffusion-main/scripts/image_sample.py --attention_resolutions 32,16,8 --class_cond False --diffusion_steps 1000 --dropout 0.1 --image_size 256 --learn_sigma True --noise_schedule linear --num_channels 256 --num_head_channels 64 --num_res_blocks 2 --resblock_updown True --use_fp16 False --use_scale_shift_norm True --model_path /home/joseangel/Documents/guided-diffusion-main/models/lsun_bedroom.pt --batch_size 4 --num_samples 1000 --timestep_respacing 1000

echo "PID of this script: $$"

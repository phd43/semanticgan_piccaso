#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffusion/samplesSR"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

#python /home/joseangel/Documents/guided-diffusion-main/scripts/super_res_sample.py  --base_samples /home/joseangel/Documents/guided-diffusion-main/output_samples/samples_10000x64x64x3_dogs_40000_timesteps.npz --model_path /home/joseangel/Documents/guided-diffusion-main/models/cifar_dogs/model040000_SR.pt --num_samples 100 --batch_size=16  --large_size 256  --small_size 64
python /home/joseangel/Documents/guided-diffusion-main/scripts/super_res_sample.py  --base_samples /home/joseangel/Documents/guided-diffusion-main/output_samples/samples_10000x64x64x3_dogs_40000_timesteps.npz --model_path /home/joseangel/Documents/guided-diffusion-main/models/cifar_dogs/model040000_SR_cosine.pt --num_samples 1000 --batch_size=16  --large_size 256  --small_size 64 --num_res_blocks 3 --learn_sigma True --class_cond False --diffusion_steps 1000

echo "PID of this script: $$"
